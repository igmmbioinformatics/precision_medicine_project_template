#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -pe sharedmem 4
#$ -l h_vmem=8G
. /etc/profile.d/modules.sh
MODULEPATH=$MODULEPATH:/exports/igmm/software/etc/el7/modules
# ------------------------------------------------------------#
#           load modules
# ------------------------------------------------------------#
module load roslin/bwa/0.7.17                   
module load igmm/apps/samtools/1.16.1
module load igmm/apps/FastQC/0.11.9
module load igmm/apps/bcftools/1.9

# ------------------------------------------------------------#
#           project params
# ------------------------------------------------------------#
genome_reference=/exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta/genome.fa

# ------------------------------------------------------------#
#           FastQC
# ------------------------------------------------------------#

# ------------------------------------------------------------#
#           Align reads
# ------------------------------------------------------------#

# ------------------------------------------------------------#
#           Variant Calling
# ------------------------------------------------------------#

