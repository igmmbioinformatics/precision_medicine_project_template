## Project Overview

In this project, you will analyze a single gene exon panel of paired end  sequencing data from a family trio. The person being studied, known as the proband, has [osteopetrosis](https://en.wikipedia.org/wiki/Osteopetrosis) which is a rare genetic disorder caused by mutations in genes responsible for the development and function of osteoclasts, cells that break down old bone tissue. There are several types of osteopetrosis caused by mutations different genes. The exon panel targets the gene [CA2](https://www.genecards.org/cgi-bin/carddisp.pl?gene=CA2) which has an known association with the disease. Both of the proband's parents do not have the disease. The goal is to identify the specific genetic variation that causes the disease. 



<figure style="text-align:center">
    <img src="figs/family.png" alt="family pedigree">
    <figcaption>Family Trio with Proband affected with Osteopetrosis.</figcaption>
</figure>



## Setup

### University HPC Eddie

Unless otherwise specified you should use the University of Edinburgh's High Performance Compute Cluster [Eddie](https://www.wiki.ed.ac.uk/display/ResearchServices/Eddie) to run the steps below.

- A quick start guide can be found [here](https://www.wiki.ed.ac.uk/display/ResearchServices/Quickstart)
- A video demonstrating how to log in using MobaXterm is available [here](https://media.ed.ac.uk/media/Introduction+to+Research+Services+-+Connecting+to+Eddie/1_jgllw76j).
- You should not run any jobs on the login nodes, instead use interactive sessions, a guide can be found [here](https://www.wiki.ed.ac.uk/display/ResearchServices/Interactive+Sessions).

Example command to start a interactive session on Eddie below:

```{console}
qlogin -l h_vmem=12G
```

All steps taken through the command line must be stored in a script. A Shell script template with the required modules can be found in the [/src/project_template.sh](/src/project_template.sh).
You should commit each stage, e.g. FastQC, in the pipeline as a separate commit to the git repository.

### Steps

1. **Clone the Git repository**: Log into the University High Performance Compute cluster Eddie  and clone the assigned Git project repository into your home folder using [git](https://swcarpentry.github.io/git-novice/aio/index.html). **Note** If you are having issues using the `ssh` methods, e.g.

```{console}
git clone git@git.ecdf.ed.ac.uk:precision_medicine_project/<username>.git
```
you can use the `https` method to download the repository on eddie.

```{console}
git clone https://git.ecdf.ed.ac.uk/precision_medicine_project/<username>.git
```

2. **Quality control**: Perform quality control of the raw reads provided in the [data](data) directory using  [FastQC](https://datacarpentry.org/wrangling-genomics/02-quality-control/index.html). 

Store the output for each sample in the [results/fastqc](results/fastqc) folder. Capture the command in the script [/src/project_template.sh](/src/project_template.sh). Then `git add` and `git commit -m 'added FastQC step'` the updated script to the git repository.

3. **Alignment**: Align reads to the human GRCh38 genome on Eddie,  `/exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta/genome.fa`, using [bwa mem](https://datacarpentry.org/wrangling-genomics/04-variant_calling/index.html) . The `bwa index` step has been  pre-compiled, for the human genome, and has been provided for you. 
```
#this step has already been done for you, do not rerun!
bwa index /exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta/genome.fa
```
Therefore, you just need to provided the path to the reference fasta file `/exports/igmm/eddie/BioinformaticsResources/igenomes/Homo_sapiens/Ensembl/GRCh38/Sequence/WholeGenomeFasta/genome.fa` as the index base. 

Save the files as `<sample_name>.sam` in the folder [results/bwa](results/bwa). Capture the command in the script [/src/project_template.sh](/src/project_template.sh). Then `git add` and `git commit -m 'added alignment step'` the updated script to the git repository.

4. **Sort and index BAM files**: Use [samtools](https://datacarpentry.org/wrangling-genomics/04-variant_calling/index.html) to convert sam files to bam files, then sort and index the BAM file. Save the files as `<sample_name>.srt.bam` in the folder [results/bwa](results/bwa). Capture the command in the script [/src/project_template.sh](/src/project_template.sh).
**Note** the BAM index file `<sample_name>.srt.bam.bai` will automatically be generated after running the `samtools index` command.
5. **Flagstat**: Run samtools flagstat on the sorted BAM file and capture the output in a file named `<sample_name>.flagstat` in the folder [results/bwa](results/bwa). Capture the command in the script [/src/project_template.sh](/src/project_template.sh).
6. **Variant calling**: Call variants for the three samples using [bcftools](https://datacarpentry.org/wrangling-genomics/04-variant_calling/index.html). Save the VCF as `<sample_name>.vcf` in the [results/vcf](results/vcf) folder. Capture the command in the script [/src/project_template.sh](/src/project_template.sh).
7. **Plotting Variants**: Write an [R](https://datacarpentry.org/genomics-r-intro/06-data-visualization/index.html) or [Python](http://swcarpentry.github.io/python-novice-gapminder/09-plotting/index.html) script to
- Read in the tab delimited VCF file [father.tsv](data/father.tsv) in the data directory. 
- Filter data for variants on chromosome 8.
- Create a scatter plot of variant position, **POS**, on the x-axis, depth , **DP** , on the y-axis and colour the points by the variant quality score, **QUAL**, remember to include a plot title and relabel the x and y axis with more meaningful names.  
- Save the R/Python script in the [src](src) folder and the plot as `father.png` in the [results/script](results/script) folder. 
8. **Annotation**: Annotate each sample's variants in the VCF file using Ensembl's online Variant Effect Predictor ([VEP](https://www.ensembl.org/info/docs/tools/vep/online/index.html)). **Note** This step is not run on Eddie. 

Save the VCF as `<sample_name>.vep.vcf`. Upload the VEP generated vcf files into the [results/vep](results/vep) folder.
Remember to give it a descriptive commit message,
<figure style="text-align:center">
    <img src="figs/upload.png" alt="How to upload to GitLab">
    <figcaption>How to upload file to GitLab.</figcaption>
</figure>

9. **Identify the disease-causing variant**: Examine the VCF file and visualize it in IGV to identify the possible disease-causing variant in the proband.
10. **IGV screenshot**: Take a screenshot in [Integrative Genomics Viewer](https://igv.org/) of the aligned reads for the father, mother, and proband over the causative variant. Save the image in the [results/IGV](results)  folder.  **Note** This step is not run on Eddie. 
11. **Documentation**: Document the software and data used in the analysis in the [docs/README.md](docs/README.md)  file.
12. **Material, Methods, and Results**: Write the material, methods, and results in the [docs/README.md](docs/README.md) file located in the docs folder.
13. **Add and commit**: Add and commit all scripts and results files to the git repository using `git add` and `git commit`. Lesson material [here](https://swcarpentry.github.io/git-novice/04-changes/index.html).
14. **Sync Remote**. Sync your git repository on eddie to your remote repository on GitLab using `git push` command. Lesson material [here](https://swcarpentry.github.io/git-novice/07-github/index.html).

To check your remote repostory is correctly setup run the command: 
```{console}
git remote -v
```
It should look something like this:
```
origin	https://git.ecdf.ed.ac.uk/precision_medicine_project/<username>.git (fetch)
origin	https://git.ecdf.ed.ac.uk/precision_medicine_project/<username>.git (push)
```

**15.** Once you are happy with your project make a note of the final commit id.



## Project Submission

Project work must be submitted back to your assigned Univeristy GitLab project by 5pm Monday 17th July 2023..


### Marking Rubric (Total: 10 points) 

1. **Git repository organization and commits (1 point)** 
- Repository is well-organized and each stage in the pipeline has a clear, descriptive commit message. 
2. **Quality control (1 point)** 
- FastQC is correctly applied, output is stored in the appropriate folder, and the script is updated. 
3. **Alignment (1 point)** 
- Reads are aligned to the human GRCh38 genome using bwa mem, output is saved correctly, and the script is updated. 
4. **Sort, index and Flagstat BAM files (1 point)** 
- SAM files are converted to BAM files, sorted, and indexed using samtools, and the script is updated. 
5. **Flagstat (1 point)** 
- Samtools flagstat is run on sorted BAM files, output is saved correctly, and the script is updated. 
6. **Variant calling (1 point)** 
- Variants are called for samples using bcftools, VCF files are saved correctly, and the script is updated. 
7. **Plotting Variants (1 point)** 
- An R or Python script is created to read in the VCF file, filter data, and create a scatter plot. Plot is saved correctly. 
8. **Annotation (1 point)** 
- VCF files are annotated using Ensembl's online VEP, output files are saved correctly. 
9. **Identification of disease-causing variant and visualized in IGV (1 point)** 
- Disease-causing variant is correctly identified in the proband and screenshot using IGV of supporting reads provided. 
10. **Documentation and writing (1 point)** 
- Software, data, material, methods, and results are clearly documented in the [docs/README.md](https://chat.openai.com/docs/README.md)  file.


## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
